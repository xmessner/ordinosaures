Le but de cette section est d'apprendre à travailler sur le pencil2 qui était un ordinateur/console compatible avec la colecovision dont il partage un certain nombre de composants matériel. L'ajout d'un adaptateur sur le port cartouche permettait de faire fonctionner les jeux de cette dernière.

Malheureusement il manque énormément d'information sur le fonctionnement de cette machine. Seul MESS(mame) permet de démarrer le bios de la machine. L'utilisation de la cartouche SD-BASIC 2.0 semble ne pas démarrer pour différentes raisons.

Cela va être pour moi l'occasion d'apprendre le Z80 ainsi que le pilotage du processeur graphique TMS9928A ainsi que de la puce sonore SN76489.

