Alors que l'ancien revient à la mode, que le retro est IN, que les vieux ordinateurs rangés au fond d'un placard prenant la poussière se voient de nouveau regagner les coeurs des anciennes générations et des nouvelles, j'ai décidé de contribuer à ce que la mémoire de ces vénérables ne soit pas perdue.

Grace à internet et aux émulateurs nous pouvons nous réjouir de retrouver nos émotions lorsque nous étions enfants, les plaisirs d'une attente interminable bercés par une musique mélodieuse s'échappant du haut parleur du téléviseur. Ce bruit strident indiquant le chargement du jeu stocké sur une simple cassette, horrible à l'époque, douce aujourd'hui.

Il ne sera question que d'une seule machine, le TO7 70. Je m'attarderais sur d'autres machines d'époque plus tard. C'est avant tout parce que ça été ma première véritable machine sur laquelle j'ai pris contacte avec l'informatique, les premiers programmes en basics recopiés de magazines. Je n'ai jamais vraiment programmé quoi que ce soit dessus. Ce n'est que très recemment que j'ai redécouvers que nous pouvions en faire quelque chose d'autre et qu'en définitive elle avait un certain potentiel. Avec les outils actuels il ne devrait pas être impossible ou trop compliqué de produire quelque chose d'intéressant. Je parle ici d'un jeu ou d'une simple présentation faite pour notre plus grand plaisir.

Je ne compte pas produire des démonstrations techniques tirant jusqu'à la dernière ressources exploitable de la machine, mais rester simple. Je souhaite faire partager les différentes étapes ainsi que les difficultés rencontrées et surtout les réussites en espérant que cela aide d'autres personnes ou même donner envie de produire.

Cela passera par l'écriture d'un programme en assembleur, le langage cible, comment le compiler sur un linux et le transférer sur un émulateur dans un premier temps. Ensuite j'attaquerai certains composants nécessaires pour rendre un programme attrayant comme dessiner à l'écran, produire du son, gérer l'interactivité via le clavier ou une manette. 

Pour y parvenir il faudra se doter de quelques outils

- Un assembleur 6809
- Un créateur de cassette
- Un convertisseur de bitmap vers un format exploitable par le T07 70
- Un émulateur

## L'assembleur

Le language de base largement utilisé sur TO7 et le basic. Facile d'accès il n'offre cependant pas énormément de capacités pour tirer partie de toute la puissance de la machine. Il en résulte une exécution assez lente du fait de l'interprêtation des commandes. L'assembleur au contraire va permettre d'accéder aux entrailles de la machine mais avec un niveau de difficulté plus important, le prix à payer pour gagner du temps et de la mémoire.

Nous allons procéder par étape. Dans un premier temps nous allons créer un programme en assembleur très simple qui ne fera qu'une seule chose comme modifier un registre, afficher quelque chose à l'écran puis rendra la main. Cela nous permettra de valider la chaine de production du binaire et son exécution dans un émulateur.

L'outil utilisé est c6809 version 0.83. 

## Création d'une K7

Le support au format cassette n'est plus utilisé depuis longtemps et est remplacé par des cartes SD, plus rapides et solides. Cependant pour l'exercice j'ai souhaité utiliser ce support pour embarquer ce qui sera produit.

Il faut de ce fait créer certains outils pour manipuler ce format. Il n'est pas question ici de brancher un lecteur cassette ou de générer un fichier WAV pour l'enregistrer sur un vrai support, mais uniquement de créer un fichier dans le bon format utilisable par les émulateurs.

Nous devrons être capable de 

- Copie d'un script basic de quelques lignes sur une k7
- Copie d'un script basic conséquent sur une k7
- Copie de plusieurs fichiers basics sur une k7
- Plusieurs fichiers au format binaire sur une K7
- Copie d'un fichier assemblé sur une k7

Avec cet outil nous pourrons alors produire depuis un linux un binaire exploitable directement depuis n'importe quel émulateur sans passer par une interface graphique pour produire la cassette.

### Format d'une K7

Le TO7 peut sauvegarder de plusieurs manières. Tout d'abord les listings en basics peuvent être retrouvés sous 2 formats, en ascii pur, c'est à dire que l'ensemble du listing est lisible. Il peut également être stocké sous forme de tokens où l'ensemble des mots clés du basics ont leur équivalence en format hexadécimal, moins gourmand en place bloc et en mémoire.
Nous pouvons également retrouver des sauvegardes de fichiers binaires. Ca peut être des zones mémoires, du code binaire assembleur ou bien n'importe quelle donnée utile non interprétable par le basic directement.

Le format d'une cassette est assez simple, nous allons essentiellement retrouver des headers permettant la séparation des différents blocs, la taille des blocs et des checksums. Voici l'organisation.

#### Basic ascii

```
### Un header de début de fichier sur 35 octets

0x00      0x09   0x0A   0x0B   0x0C   0x0D                0x18   0x19   0x1A   0x1B        0x21  0x22
0            9     10     11     12     13                  24     25     26     27          33    34
|            |      |      |      |      |                   |      |      |      |           |     |
-----------------------------------------------------------------------------------------------------
| 0xFF (10*) | 0x01 | 0x3c | 0x00 | 0x14 | NOM?????.BAS (12) | 0x00 | 0xFF | 0xFF | 0x00 (6*) | CRC |
-----------------------------------------------------------------------------------------------------
                           [---------------------- CRC ---------------------------]

### Un header de bloc de 15 octets, puis le bloc et le crc (premier bloc)

0x00      0x09   0x0A   0x0B   0x0C          0x0D   0x0E            
0            9     10     11     12            13     14                   14+x  15+x
|            |      |      |      |             |      |                      |     |
-------------------------------------------------------------------------------------
| 0xFF (10*) | 0x01 | 0x3c | 0x01 | TAILLE BLOC | Ox0D | BLOC 0 -> 254 octets | CRC |
-------------------------------------------------------------------------------------
                                                       [-------- CRC ---------]

### Un header de bloc de 14 octets, puis le bloc et le crc (second bloc et suivants)

0x00      0x09   0x0A   0x0B   0x0C          0x0D
0            9     10     11     12            13                   13+x  14+x
|            |      |      |      |             |                      |     |
------------------------------------------------------------------------------
| 0xFF (10*) | 0x01 | 0x3c | 0x01 | TAILLE BLOC | BLOC 0 -> 254 octets | CRC |
------------------------------------------------------------------------------
                                                [-------- CRC ---------]

### Un footer de 15 octets pour signaler la fin du bloc

0x00      0x09   0x0A   0x0B   0x0C   0x0D   0x0E
0            9     10     11     12     13     14
|            |      |      |      |      |      |
-------------------------------------------------
| 0xFF (10*) | 0x01 | 0x3c | 0xFF | 0x00 | 0xFF |
-------------------------------------------------

```

#### Basic token

### Binaire

## Extracteur de fichiers d'une K7

## Les bitmaps

### Le format de l'écran

Sur les anciens ordinateurs la notion de bitmap est différente. Il était commun à cet époque d'avoir une zone mémoire dédiée à l'écran mais où un octet ne correspondait pas nécessairement à un pixel affiché. Pour le TO7 nous avons une zone mémoire spécifique pour la gestion de ce que nous appelons la forme. C'est à dire si un pixel doit être alumé ou éteint. La représentation est de ce fait binaire. De plus nous ne pouvons pas adresser directement un pixel mais un groupe de pixel. Cela ne rend pas la tâche facile lorsque nous souhaitons afficher uniquement un seul pixel ou bien le déplacer. Voici comment elle est organisée

```
 7 6 5 4 3 2 1 0
-----------------
|.|.|.|.|.|.|.|.|
-----------------
```

Pour afficher un pixel alumer nous allons travailler en réalité sur un octet puis passer à 1 l'un de ses bits. Par exemple pour afficher le premier pixel à gauche il faut en binaire ceci

```
 7 6 5 4 3 2 1 0
-----------------
|1|0|0|0|0|0|0|0|
-----------------
```
La suite ```1000 0000``` devient en réalité 0x80. En positionnant cet octet à l'adresse de début de l'écran nous aurons les 8 premiers pixels d'activés ou de désactivés en fonction de l'état des bits.

Ici nous n'avons que la mémoire de forme, il nous faut maintenant colorier notre pixel. C'est ici que cela se complique un peu. L'organisation de l'écran ne permet pas de colorier plusieurs pixels consécutifs mais uniquement de positionner 2 couleurs par blocs de 8 bits. Par exemple les 16 couleurs du TO7 70 ne peuvent pas être affichées sur 16 pixels consécutifs. Il existe une contrainte imposant de n'avoir que 2 couleurs possibles sur un bloc de 8 pixels (bit).

** EXPLIQUER comment choisir la couleur **

### L'accès à la mémoire écran en basic

### L'accès à la mémoire écran en assembleur

L'accès à la mémoire écran de forme se fait en passant à 1 l'octet à l'adresse $E7C3. Ensuite lorsque nous voudrons positionner un pixel, nous n'aurons plus qu'a écrire à partir de l'adresse $4000. Le programme ci dessous affiche des lignes verticales sur l'ensemble de l'écran

```
(main)MAIN.ASM   * Marquage du programme principal
	org	$8000
	lda	#$01         * Chargement dans a de la valeur 0x01
	ora	$E7C3        * OU sur l'octet à l'adresse du choix forme/couleur
	ldx	#$4000       * Chargement de l'adresse écran
bou	lda	#$AA         * Charge dans a la valeur 0xAA
	sta	,x+          * envoie la valeur à l'adresse chargée dans x et l'incrémente de 1
	cmpx	#$5F40       * Compare x à la valeur de fin de l'écran
	bne	bou          * Tant que pas égale, on boucle
	rts                  * Retour au programme principal
```

## L'émulation

Actuellement l'émulateur que j'utilise est DCMOTO sous Linux avec Wine, ou directement depuis Windows. Je ne l'utilise que pour l'émulation TO7 70. Il apporte un certain nombre d'outils pour l'aide au développement, comme un désassembleur, une exploration de la mémoire, l'injection de code à une adresse donnée ...

Cela va nous faire gagner du temps pour trouver ce qui ne fonctionnera pas dans notre code et vérifier très rapidement ce qu'il se passe.

## Divers

### DEBUG

Lors de la phase de développement de l'utilitaire de gestion de la cassette je me suis retrouvé face à un problème, le debogage. Lorsque nous programmons en C l'utilisation de pointeurs, de structures, de zone mémoire est nécessaire. Quand le programme plante ou ne fait pas ce que nous souhaitons, on se retrouve sans autre possibilité de reprendre le source, rajouter des affichages de variables, de textes pour savoir où peut bien se trouver le bug qui en général se cache très bien.

Heureusement on a pour nous aider dans cette tâche un outil puissant qui est **GDB**. Alors évidemment, c'est un outil puissant, mais également peu intuitif lorsqu'on ne l'utilise pas souvent ou qu'on ne sait pas trop ce qu'il peut nous apporter.

Je ne vais pas ici faire un cours magistrale sur ce qu'est un debugger ou comment utiliser les options les plus puissantes de l'outil, mais simplement donner les clés qui m'ont bien aidées dans ma tâche.

Voici quelques commandes utiles qu'il est bon de connaître 

- Lancement : gdb binaire
- set args <arguments> : positionne les arguement comme si c'était sur la ligne de commande
- start : démarre le programme au point d'entrée _main_
- run : lance le programme jusqu'à un point d'arrêt, une erreur, ou jusqu'à la fin du programme
- display : rajoute une variable à afficher à chacun des pas du programme
- next / n : passe à la l'instruction suivante mais ne rentre pas dans les fonctions
- step / s : passe à l'instruction suivante et entre dans les fonctions
- print <var> : affice une variable
- print \*<var> : Affiche le contenu d'une structure
- list : affiche le code source à l'endroit de l'exécution
- break <fonction> : pose un point d'arrêt sur une fonction

### Compilation & gestion de la mémoire

Lorsque nous compilons un programme en C il est très simple de lui faire faire pratiquement n'importe quoi. Le langage étant très permissif au niveau de la syntaxe et des type par défaut. Dans un  premier temps pour éviter les erreurs sur les type nous utiliserons lors de la compilation un certain nombre de flags pour nous imposer à plus de rigeur. Le but étant de n'avoir aucun warning. Ce sera déjà un pas vers moins de bugs.

Ensuite si nous devons manipuler des reservations mémoire, il conviendra d'utiliser **Valgrind**. C'est un outil de vérification des allocations qui va s'assurer qu'une allocation faite et bien restituée au système, que l'écriture dans une zone mémoire ne dépasse pas la limite de la réservation initiale. Cela peut paraître évident, mais il est très facile de se louper sur un indexe, sur une taille mal calculée ou mal testée. La punition est pas systématiquement visible aux premiers lancement. Il se peut que cela arrive après plusieurs modifications où et la retrouver peut demander du temps.

```
QUELQUES EXEMPLES
```


